import numpy as np
from numpy.polynomial.legendre import leggauss
import matplotlib.pyplot as plt
from scipy.stats import norm
import copy

Q = 0
R = 10
sigma = 1/np.sqrt(R)
print(sigma)
dt = 0.1
N = 10
xlim=[-2, 2]
degree=30
num_x0=100
x0_arr = np.linspace(xlim[0],xlim[1],num_x0)
grid_width = 4

def q(x):
    return Q*(x-1)**2/2

# dx = (-x^3 + x +u )dt + sigma dw
def b(x):
    return -x**3 + x
def bx(x):
    return -3*x**2 + 1
def intb(x):
    return -1/4*x**4 + 1/2*x**2

def w_cost(x):
    V = q(x) + R / 2 * b(x) ** 2 - (sigma ** 2 * R / 2) * bx(x)
    return np.exp(-V / (sigma ** 2 * R) * dt)
def w_N(x):
    return np.exp(-(q(x)-R*intb(x))/(sigma**2*R))

def check_symmetric(a, tol=1e-8):
    return np.allclose(a, a.T, atol=tol)

# specify weights
[xi, alpha] = leggauss(degree)
xi = np.array([xi]).T
alpha = np.array([alpha]).T

f_arr = np.zeros(num_x0)

for k in range(len(x0_arr)):
    print(k)
    x0=x0_arr[k]
    xlim_np1 = np.array([-grid_width*sigma*np.sqrt(dt),grid_width*sigma*np.sqrt(dt)]) + x0
    xi_np1 = (xlim_np1[1] - xlim_np1[0]) / 2 * xi + (xlim_np1[0] + xlim_np1[1]) / 2
    a=norm.pdf(xi_np1,x0,sigma * np.sqrt(dt))
    Phi0=w_cost(xi_np1) * norm.pdf(xi_np1,x0,sigma * np.sqrt(dt))
    Phi_n = np.eye(degree)
    for n in range(1,N):
        # specify weights
        xi_n = copy.copy(xi_np1)
        alpha_n = alpha * (xlim_np1[1] - xlim_np1[0]) / 2
        xlim_np1 = np.array([-grid_width * sigma * np.sqrt((n+1)*dt), grid_width * sigma * np.sqrt((n+1)*dt)]) + x0

        xi_np1 = (xlim_np1[1] - xlim_np1[0]) / 2 * xi + (xlim_np1[0] + xlim_np1[1]) / 2
        Xi_n = np.array([xi_np1-xi_n[i] for i in range(degree)])[:,:,0]

        gamma = alpha_n * w_cost(xi_n)
        Gamma = np.diag(gamma[:,0])
        Phi = norm.pdf(Xi_n,0,sigma * np.sqrt(dt))
        Phi_n = np.matmul(np.matmul(Phi, Gamma),Phi_n)


    alpha_N = alpha * (xlim_np1[1] - xlim_np1[0]) / 2
    gamma_N = alpha_N * w_N(xi_np1)
    Phi_final = np.matmul(Phi_n,gamma_N)
    f_arr[k] = (Phi_final.T).dot(Phi0)[0,0]

print(xlim_np1-x0)
plt.plot(x0_arr,f_arr)
plt.show()