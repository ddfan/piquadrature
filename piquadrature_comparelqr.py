import numpy as np
from numpy.polynomial.hermite import hermgauss
import matplotlib.pyplot as plt
from scipy.stats import norm

Q = 2
R = 1
sigma = 1/np.sqrt(R)
print('sigma: ', sigma)
dt = 0.1
N = 10
xlim=[-2, 2]
degree=100
xi_scale = sigma*np.sqrt(N*dt)/2

def q(x):
    return Q*x**2/2

def qx(x):
    return Q*x

# dx = (-x^3 + x +u )dt + sigma dw
def b(x):
    return x#-x**3 + x
def bx(x):
    return 1#-3*x**2 + 1
def bxx(x):
    return 0#-6*x
def intb(x):
    return 1/2*x**2#-1/4*x**4 + 1/2*x**2

def w_cost(x):
    V = q(x) + R / 2 * b(x) ** 2 - (sigma ** 2 * R)/2 * bx(x)
    return np.exp(-V / (sigma ** 2 * R) * dt)
def w_N(x):
    return np.exp(-(q(x)-R*intb(x))/(sigma**2*R))
def w_cost_x(x):
    return w_cost(x)*(-1/(sigma ** 2 * R) * dt * (qx(x) + R*b(x)*bx(x) - (sigma ** 2 * R)/2 * bxx(x)))

def check_symmetric(a, tol=1e-8):
    return np.allclose(a, a.T, atol=tol)

# specify weights
[xi, alpha] = hermgauss(degree)
xi = np.array([xi]).T
alpha = np.array([alpha]).T / np.sqrt(np.pi)
xi = xi_scale * np.sqrt(2) * xi
alpha = alpha / norm.pdf(xi,0,xi_scale)
print(xi.T,alpha.T)
print('quadrature limits: ', np.min(xi),np.max(xi))
Xi = np.array([xi-xi[i] for i in range(degree)])[:,:,0]
gamma = alpha * w_cost(xi)
Gamma = np.diag(gamma[:,0])
# Gamma = np.diag(alpha[:,0])
gamma_N = alpha * w_N(xi)
Phi =norm.pdf(Xi,0,sigma * np.sqrt(dt))
Phi2 = np.matmul(Phi,Gamma)

print(gamma_N.T.dot(norm.pdf(xi, 0, sigma * np.sqrt(dt))))

num_x0=1000
num_bins=100
x0_arr = np.linspace(xlim[0],xlim[1],num_x0)
f_arr = np.zeros((num_x0,N-1))
u_arr = np.zeros((num_x0,N-1))
b_arr = np.zeros(num_x0)
x_arr = np.zeros((num_x0,N))
x_bins = np.zeros((num_bins,N-1))
x_arr[:,0]=x0_arr

# Phi3 = np.linalg.matrix_power(Phi2, 1)
# Phi3=np.eye(degree)
# Phi4 = np.matmul(Phi3, gamma_N)
for n in range(N-1):
    # print(n)
    x_bins[:,n] = np.histogram(x_arr[:, n],bins=num_bins, range=(xlim[0],xlim[1]))[0]/num_x0
    Phi3 = np.linalg.matrix_power(Phi2, N-2-n)
    Phi4 = np.dot(gamma_N.T,Phi3).T

    for k in range(num_x0):
        x0=x_arr[k,n]
        Phi0 = norm.pdf(xi, x0, sigma * np.sqrt(dt))
        f_arr[k,n] = (Phi4.T @ Phi0)[0,0] * w_cost(x0)
        u_arr[k,n] = -b(x0) + sigma**2 * ((Phi4.T).dot(w_cost(x0)*Phi0*(xi-x0)/(sigma**2*dt))[0,0] + (Phi4.T).dot(w_cost_x(x0)*Phi0) ) / f_arr[k,n]
    x_arr[:,n+1] = np.sort(x_arr[:,n] + (b(x_arr[:,n]) + u_arr[:,n])*dt + sigma*np.sqrt(dt)*np.random.randn(num_x0))

b_arr[:] = b(x_arr[:,0])
A=b(1)
B=1
P=np.zeros(N-1)
F=np.zeros(N-1)
P[-1]=Q
for n in range(N-2,0,-1):
    P[n-1] = (A*P[n]+P[n]*A-(P[n]*B)/R*(B*P[n])+Q)*dt+P[n]
    F[n] = B*P[n]/R
F[0]=B*P[0]/R
print(P)
u_lqr = -F*x_arr[:,:-1]
v=(x_arr[:,:-1]**2)*P/2
f_lqr=np.exp(-(v-R*intb(x_arr[:,:-1]))/(sigma**2*R))

ax=plt.subplot(411)
ax.set_prop_cycle('color', [plt.cm.viridis(j) for j in np.linspace(0, 1, N)])
plt.plot(x_arr[:,:-1],f_arr)
# ax.set_prop_cycle('color', [plt.cm.magma(j) for j in np.linspace(0, 1, N)])
# plt.plot(x_arr[:,:-1],f_lqr)
plt.ylabel('f(t,x)')
plt.xlim(xlim[0],xlim[1])
ax=plt.subplot(412)
# ax.set_prop_cycle('color', [plt.cm.viridis(j) for j in np.linspace(0, 1, N)])
# plt.plot(x_arr[:,:-1],f_arr)
ax.set_prop_cycle('color', [plt.cm.magma(j) for j in np.linspace(0, 1, N)])
plt.plot(x_arr[:,:-1],f_lqr)
plt.ylabel('f(t,x) (lqr)')
plt.xlim(xlim[0],xlim[1])
ax=plt.subplot(413)
ax.set_prop_cycle('color', [plt.cm.viridis(j) for j in np.linspace(0, 1, N)])
plt.plot(x_arr[:,0],b_arr,'r')
plt.plot(x_arr[:,:-1],u_arr)
ax.set_prop_cycle('color', [plt.cm.magma(j) for j in np.linspace(0, 1, N)])
plt.plot(x_arr[:,:-1],u_lqr)
plt.xlim(xlim[0],xlim[1])
plt.ylabel('u(t,x)')
ax=plt.subplot(414)
ax.set_prop_cycle('color', [plt.cm.viridis(j) for j in np.linspace(0, 1, N)])
xplot = np.linspace(xlim[0],xlim[1],num_bins)
plt.plot(xplot,x_bins)
p_check = np.exp(-2/sigma**2*(xplot**4/4-xplot**2/2))
p_check = p_check / np.sum(p_check)
plt.plot(xplot,p_check,'red')
plt.xlim(xlim[0],xlim[1])
plt.ylabel('p(t,x)')

plt.show()
