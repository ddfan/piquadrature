import numpy as np
from numpy.polynomial.legendre import leggauss
import matplotlib.pyplot as plt
from scipy.stats import norm

Q = 0
R = 1
sigma = 1/np.sqrt(R)
print(sigma)
dt = 0.1
N = 100
xlim=[-5, 5]
degree=300

def q(x):
    return Q*(x)**2/2

# dx = (-x^3 + x +u )dt + sigma dw
def b(x):
    return 2*x
def bx(x):
    return 2
def intb(x):
    return x**2

def w_cost(x):
    V = q(x) + R / 2 * b(x) ** 2 - (sigma ** 2 * R / 2) * bx(x)
    return np.exp(-V / (sigma ** 2 * R) * dt)
def w_N(x):
    return np.exp(-(q(x)-R*intb(x))/(sigma**2*R))

def check_symmetric(a, tol=1e-8):
    return np.allclose(a, a.T, atol=tol)

# specify weights
[xi, alpha] = leggauss(degree)
xi = np.array([xi]).T
alpha = np.array([alpha]).T
xi = (xlim[1] - xlim[0]) / 2 * xi + (xlim[0] + xlim[1]) / 2
alpha = alpha * (xlim[1] - xlim[0]) / 2

Xi = np.array([xi-xi[i] for i in range(degree)])[:,:,0]
gamma = alpha * w_cost(xi)
Gamma = np.diag(gamma[:,0])
gamma_N = alpha * w_N(xi)
Phi =norm.pdf(Xi,0,sigma * np.sqrt(dt))

Phi2 = np.matmul(Phi,Gamma)

num_x0=2000
num_bins=200
binlim = [-10,10]
x0_arr = np.linspace(xlim[0],xlim[1],num_x0)
f_arr = np.zeros((num_x0,N-1))
u_arr = np.zeros((num_x0,N-1))
x_arr = np.zeros((num_x0,N))
x_bins = np.zeros((num_bins,N-1))
x_arr[:,0]=x0_arr
for n in range(N-1):
    print(n)
    x_bins[:,n] = np.histogram(x_arr[:, n],bins=num_bins,range=binlim)[0]/num_x0
    Phi3 = np.linalg.matrix_power(Phi2, N-1-n)
    Phi4 = np.matmul(Phi3, gamma_N)

    for k in range(num_x0):
        x0=x_arr[k,n]
        Phi0=w_cost(xi) * norm.pdf(xi,x0,sigma * np.sqrt(dt))
        f_arr[k,n] = (Phi4.T).dot(Phi0)[0,0]
        u_arr[k,n] = sigma**2 * (Phi4.T).dot(Phi0*(xi-x0)/sigma**2)[0,0] / f_arr[k,n]

    x_arr[:,n+1] = np.sort(x_arr[:,n] + (b(x_arr[:,n]) + u_arr[:,n])*dt + sigma*np.sqrt(dt)*np.random.randn(num_x0))

ax=plt.subplot(311)
ax.set_prop_cycle('color', [plt.cm.viridis(j) for j in np.linspace(0, 1, N)])
plt.plot(x_arr[:,:-1],f_arr)
plt.ylabel('f(t,x)')
plt.xlim(xlim[0],xlim[1])
ax=plt.subplot(312)
ax.set_prop_cycle('color', [plt.cm.viridis(j) for j in np.linspace(0, 1, N)])
plt.plot(x_arr[:,:-1],u_arr)
plt.xlim(xlim[0],xlim[1])
plt.ylabel('u(t,x)')
ax=plt.subplot(313)
ax.set_prop_cycle('color', [plt.cm.viridis(j) for j in np.linspace(0, 1, N)])
xplot = np.linspace(binlim[0],binlim[1],num_bins)
plt.plot(xplot,x_bins)
p_check = np.exp(-2/sigma**2*(-xplot**2/2))
p_check = p_check / np.sum(p_check)
# plt.plot(xplot,p_check,'red')
# plt.xlim(xlim[0],xlim[1])
# plt.ylabel('p(t,x)')
plt.show()

